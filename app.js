var express = require('express');
var app = module.exports = express();
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var orm = require('orm');
var expressValidator = require('express-validator');
var busboy = require('connect-busboy');
// passport
var expressSession = require('express-session');
var passport = require('passport');
app.passport = passport;

// database settings
app.models = {};
app.db     = {};
var database = require('./config/database');

app.use(orm.express(database.opts, {
	define: function (db, models){
		database.define(db, app);
		app.models = db.models;
		app.db = db;
		db.sync(function(err,res){
			if (err) {
				throw(err);
			}
		});
	}
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mustache');
app.engine('mustache', require('hogan-middleware').__express);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(busboy());

// Configuring Passport
require('./config/passport')(app,passport);
app.use(expressSession({secret: 'ezshare-key'}));
app.use(passport.initialize());
app.use(passport.session());

// controllers
require('./app/controllers/')(app);


// routes
var routes = require('./routes/index');
var ez_user = require('./routes/ez_user')(app);
var ez_file = require('./routes/ez_file')(app);

// views
app.use('/', routes);
app.use('/share', routes);
app.use('/download', routes);

// api
app.use('/ez_user',ez_user);
app.use('/ez_file',ez_file);


// JSON error handler
app.use(function (err, req, res, next){
	req.unhandledError = err;
	var message = err.message;
	var error = err.error || err;
	var status = err.status || 200;
	res.json({message: message, error: error}, status);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
