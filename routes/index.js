var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/download', function(req, res, next) {
  res.render('download', { title: 'Express' });
});

router.get('/share', function(req, res, next) {
  res.render('share', { title: 'Express' });
});

module.exports = router;
