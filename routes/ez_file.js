module.exports = function (app) {
	var express = require('express');
	var router = express.Router();
	var Q = require('q');

	router.post('/', app.passport.authenticate('localapikey') ,function(req, res, next) {
		app.controllers.EzUser.findByApikey(req.query.apikey)
		.then(function (user) {
			req.body.user = user;
			return app.controllers.EzFile.share(req);
		})
		.then(function (file) {
			res.send(file);
		})
		.fail(function (err) {
			return next({ message:"error sharing", error:err});
		});
	});

	router.get('/:code', function(req, res, next) {
		app.controllers.EzFile.serve(req.params.code)
		.then(function (filepath) {
			var path = require('path');
			var mime = require('mime');
			var fs = require('fs');
			var file = filepath;
			var filename = path.basename(file);
			var mimetype = mime.lookup(file);
			res.setHeader('Content-disposition', 'attachment; filename=' + filename);
			res.setHeader('Content-type', mimetype);
			var stream = fs.createReadStream(file);
			res.download(file);
		})
		.fail(function (err){
			return next({ message:"nothing to serve", error:err});
		});
	});

	return router;
};
