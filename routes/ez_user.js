module.exports = function (app) {
	var express = require('express');
	var router = express.Router();
	var Q = require('q');

	router.post('/', function(req, res, next) {
		app.controllers.EzUser.create(req)
		.then(function (user) {
			res.send(user);
		})
		.fail(function (err) {
			return next({ message:"error creating user", error:err});
		});
	});

	return router;
};
