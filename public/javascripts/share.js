function setApikey(key) {
	window.localStorage.setItem("ez_share_apikey", key);
};

function getApikey() {
	return window.localStorage.getItem("ez_share_apikey");
};

function log () {
	console.log("log");
	$.ajax({
		url:SITE+'/ez_user/',
		type:'POST',
		dataType:'json',
		data:{
			login:$('input[name=login]').val(),
			password:$('input[name=password]').val(),
		},
		success:function(data) {
			console.log("func"+JSON.stringify(data));
			if (data.error) {
				$('#mainWindows').append('error');
			}
			setApikey(data.apikey);
			location.reload();
		}
	});
}

function uploadFile() {
	console.log("log");
	var formData = new FormData($('form')[0]);
	$.ajax({
		url:SITE+'/ez_file?apikey='+getApikey(),
		type:'POST',
		dataType:'json',
		data:formData,
		cache: false,
        contentType: false,
        processData: false,
		success:function(data) {
			console.log("func"+JSON.stringify(data));
			if (data.error) {
				$('#mainWindows').append('error');
			} else {
				$('#mainWindows').append('LINK: '+SITE+'/ez_file/'+data.code);
			}
		}
	});	
}

$(document).ready( function () {
	var key = getApikey();
	console.log("key"+key);
	if (key != undefined && key != '' && key != 'undefined') {
		console.log("logged");
		$('#unlogged').remove();

		var hidden = '<form enctype="multipart/form-data">';
		hidden +='<input type="file" name="file"><br>';
		hidden +='<input type="submit" value="Share" onclick="uploadFile(); return false;">';
		hidden +='</form>';

		$('#mainWindows').append(hidden);
	}
});

