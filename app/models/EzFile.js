module.exports = function(db, cb){
	db.define('ez_file', {
		ez_user_id		: { type: "integer"},
		date_creation	: { type: "date"},
		code			: { type: "text"},
		name			: { type: "text"}
    });

	return cb();
};
