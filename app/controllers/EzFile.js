module.exports = function(app) {
	var module = {};
	var Q			= require("q");
	var fs			= require('fs');
	var crypto = require('crypto');

	function genCode (howMany, chars) {
		chars = chars 
			|| "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		var rnd = crypto.randomBytes(howMany)
			, value = new Array(howMany)
			, len = chars.length;

		for (var i = 0; i < howMany; i++) {
			value[i] = chars[rnd[i] % len]
		};

		return value.join('');
	}

	module.share = function(req) {
		var deferred = Q.defer();
		var tempDir = __dirname + '/../../sharedFolder/';
		var fstream;

		req.pipe(req.busboy);
		req.busboy.on('file', function (fieldname, file, filename) {
			fstream = fs.createWriteStream(tempDir + filename);
			file.pipe(fstream);
			console.log("coucou");
			fstream.on('close', function () {
				app.models.ez_file.create({
					ez_user_id:req.body.user.id,
					code:genCode(6),
					date_creation:new Date(),
					name:filename,
				}, function (err, newFile) {
					console.log("coucou");
					if (err) {
						deferred.reject(err);
					} else {
						deferred.resolve(newFile);
					}
				});
			});
		});
		return deferred.promise;
	};

	module.serve = function(code) {
		var deferred = Q.defer();
		app.models.ez_file.find({code:code}, function (err, res) {
			if (err) {
				deferred.reject(err);
			} else {
				if (res.length > 0) {
					var now = new Date();
					var nowMinusWeek = new Date();
					nowMinusWeek.setDate(nowMinusWeek.getDate() - 1);
					if (res[0].date_creation < nowMinusWeek) {
						deferred.reject("expired");
					} else {
						var path = __dirname+'/../../sharedFolder/'+res[0].name;
						deferred.resolve(path);
					}
				} else {
					deferred.reject("there is nothing");
				}
			}
		});
		return deferred.promise;
	};

	return module;
};
