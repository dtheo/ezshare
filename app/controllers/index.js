module.exports = function(app) {
	app.controllers = {};
	app.controllers.EzUser = require('./EzUser')(app);
	app.controllers.EzFile = require('./EzFile')(app);
};
