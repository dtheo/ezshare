module.exports = function(app) {
	var modules = {}
	var Q = require('q');
	var validator = require('express-validator');
	var bcrypt = require('bcrypt-nodejs');
	var hat = require('hat');

	modules.create = function(req) {
		var deferred = Q.defer();
		
		console.log('coucou');
		req.assert('login','Votre login doit être non vide').notEmpty();
		req.assert('password','Votre mot de passe doit être non vide').notEmpty();
		var errors = req.validationErrors();
		
		console.log('coucou');
		if (errors) {
			deferred.reject(errors);
			return deferred.promise;
		}

		console.log('coucou');

		app.models.ez_user.create({
			login:req.body.login,
			password:bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null),
			apikey:hat()
		}, function (err, newUser) {
			console.log('coucou');
			if (err) {
				deferred.reject(err);
			} else {
				deferred.resolve(newUser);
			}
		});

		return deferred.promise;
	};

	modules.findByApikey = function(apikey) {
		var deferred = Q.defer();
		app.models.ez_user.find({apikey:apikey}, function(err, res) {
			if (err) {
				deferred.reject(err);
			} else {
				if (res.length > 0) {
					deferred.resolve(res[0]);
				} else {
					deferred.reject("user not found");
				}
			}
		});

		return deferred.promise;
	};

	return modules;
};
