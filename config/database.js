module.exports.opts = {
	database : "ezshare",
	protocol : "mysql",
	host     : "localhost",
	port     : 3306,         
	user     : "root",
	password : "root"
};

module.exports.define = function(db, app){
	db.load("../app/models/EzUser", function (err) {  
		if (err) {
			console.log(err);
		}
		app.models.ez_user = db.models.ez_user; 
	});
	db.load("../app/models/EzFile", function (err) {  
		if (err) {
			console.log(err);
		}
		app.models.ez_file = db.models.ez_file; 
	});

	app.models.ez_file.hasOne("ez_user", app.models.ez_user);

};

