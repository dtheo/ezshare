var ApiKeyStrategy = require('passport-localapikey').Strategy;
module.exports = function (app, passport) {
	passport.serializeUser(function(user, done) {
		done(null,user.id);
	});

	passport.deserializeUser(function(id, done) {
		app.models.ez_user.find( {id:id}, function(err, res) {
			done(err,res);
		});
	});

	// configuration module passport pour la stratégie API KEY
	passport.use(new ApiKeyStrategy(
		function(apikey, done) {
			app.models.ez_user.find({apikey:apikey}, function (err, user) {
				if (!user[0]) { return done(null, false); }
				return done(null, user[0]);
			});
		}
	));
};
